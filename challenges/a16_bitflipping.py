from random import randint
from  Crypto.Cipher import AES
from a09_pkcs_padding import pad, unpad
from a11_detection_oracle import generate_key
from utils import chunk

KEY = generate_key()
IV = generate_key()
aes = AES.new(KEY, AES.MODE_CBC, IV)
PREFIX = "comment1=cooking%20MCs;userdata="
SUFIX = ";comment2=%20like%20a%20pound%20of%20bacon"

def encrypt(s):
    return aes.encrypt(pad(s))

def decrypt(s):
    return unpad(aes.decrypt(s))

def clean(s):
    s = s.replace(';', '%3B')
    s = s.replace('=', '%3D')
    return s

def encrypt_with_additional_data(s):
    return encrypt(PREFIX + clean(s) + SUFIX)

def detect(pt):
    data = map(lambda s: tuple(s.split('=')), pt.split(';'))
    return "admin" in [i[0] for i in data]

def sxor(s1, s2):
    return ''.join(map(lambda s: chr(ord(s[0]) ^ ord(s[1])), zip(s1, s2)))

def modify_block(s, b):
    if len(s) < len(b):
        return sxor(s, b) + b[len(s):]
    return sxor(s, b)

def modify(ct):
    wanted = ";admin=true;"
    pt_block = chunk(PREFIX, AES.block_size)[1]
    modification_string = sxor(wanted, pt_block)
    modified = modify_block(modification_string, ct[:AES.block_size])
    return modified + ct[AES.block_size:]

if __name__ == "__main__":
    s = "abc"
    ct = encrypt_with_additional_data(s)
    print "Has admin field before modification: ", detect(s)
    modified = modify(ct)
    pt = decrypt(modified)
    print "Has admin field after modification: ", detect(pt)
