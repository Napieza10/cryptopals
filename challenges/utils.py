from string import ascii_lowercase as LETTERS
from os import urandom
from random import randint

def s2b(s):
    return ''.join(map(lambda x: bin(ord(x))[2:].zfill(8), s))

def chunk(s, size, count=None):
    return [s[i: i + size] for i in xrange(0, len(s), size)][:count]

def write(filename, data):
    with open(filename, 'w') as f:
        f.write(data)

def unique(s):
    return list(set(s))

def remove_extra(s):
    extras = set(s).difference(set(LETTERS))
    return filter(lambda x: not (x in extras), s)

def frequency(text):
    chars = unique(text)
    chars = remove_extra(chars)
    return {char : text.count(char) / float(len(text)) for char in chars}

def frequency_deviation(f):
    KNOWN_FREQUENCIES = { 'e' : 12.02, 't' : 9.10, 'a' : 8.12, 'o' : 7.68, 'i' : 7.31, 'n' : 6.95, 's' : 6.28, 'r' : 6.02, 'h' : 5.92, 'd' : 4.32,
                          'l' : 3.98, 'u' : 2.88, 'c' : 2.71, 'm' : 2.61, 'f' : 2.30, 'y' : 2.11, 'w' : 2.09, 'g' : 2.03, 'p' : 1.82, 'b' : 1.49,
                          'v' : 1.11, 'k' : 0.69, 'x' : 0.17, 'q' : 0.11, 'j' : 0.10, 'z' : 0.07}
    DECIMAL_POINTS = 2
    frequencies = frequency(f)
    return round(sum(map(lambda x: abs(frequencies[x] - KNOWN_FREQUENCIES[x]), frequencies)), DECIMAL_POINTS)

def sort_by_deviations(pt):
    deviations = map(frequency_deviation, pt)
    corr = {p : d for p, d in zip(pt, deviations)}
    return sorted(corr, key=corr.get)

def generate_prefix():
    length = randint(1, 15)
    return urandom(length)

def modexp(x, e, n):
    s = 1
    while e != 0:
        if e & 1 == 1:
            s = (s * x) % n
        e >>= 1
        x = (x * x) % n
    return s

def bxor(s, b):
    return ''.join(map(lambda c: chr(ord(c) ^ b), s))

def sxor(s1, s2):
    return ''.join(map(lambda s: chr(ord(s[0]) ^ ord(s[1])), zip(s1, s2)))
