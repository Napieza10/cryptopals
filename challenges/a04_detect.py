from single_byte_xor import decrypt
import string

def is_printable_char(c):
    return c in string.printable

def is_printable(s):
    return all([is_printable_char(c) for c in s])

def find_plaintext(filename):
    possible_plaintexts = decrypt_strings(filename)
    return filter(is_printable, possible_plaintexts)

def decrypt_strings(filename):
    with open(filename, 'r') as f:
        ciphertexts = f.read().strip().split('\n')
        return [decrypt(s) for s in ciphertexts]

if __name__ == "__main__":
    plaintext = find_plaintext('4.txt')
    open('plaintext_4.txt', 'w').write('\n'.join(plaintext))
