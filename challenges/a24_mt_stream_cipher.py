from a21_rng import MT
from utils import sxor
from random import randint
from os import urandom

BYTES_IN_WORD = 4
BITS_IN_BYTE = 8

NUMBER_OF_A = 14

def seed(s):
    if s > 0xffff:
        raise "The seed value must be 16 bits long"
    G.seed()

def bytes_from_word(n):
    BYTE_MASK = (1 << BITS_IN_BYTE) - 1
    words = [((BYTE_MASK << BITS_IN_BYTE * i) & n) >> BITS_IN_BYTE * i for i in xrange(BYTES_IN_WORD)]
    return words[::-1]

def keystream(s, n):
    G = MT()
    G.seed(s)

    count = n // BYTES_IN_WORD + 1

    keywords = [bytes_from_word(G.next()) for i in xrange(count)]
    return reduce(lambda x, y: x + y, keywords, [])

def encrypt(s, seed):
    k = keystream(seed, len(s))
    return sxor(s, ''.join([chr(c) for c in k]))

def decrypt(ct, seed):
    return encrypt(ct, seed)

def encrypt_known_plaintext():
    NUMBER_OF_RAND_CHARS = randint(1, 16)
    pt = urandom(NUMBER_OF_RAND_CHARS) + 'A' * NUMBER_OF_A
    seed = randint(1, 0xffff)
    print "SEED IS: ", seed
    return encrypt(pt, seed)

def recover_seed(ct):
    rand_sequence_size = len(ct) - NUMBER_OF_A
    for seed in xrange(0xffff):
        if decrypt(ct, seed)[-NUMBER_OF_A:] == 'A' * NUMBER_OF_A:
            return seed
    return None

if __name__ == "__main__":
    ct = encrypt_known_plaintext()
    print recover_seed(ct)
