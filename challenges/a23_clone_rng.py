from a21_rng import MT, n as CYCLE, w as WORD

def lo_mask(l):
    return (1 << l) - 1

MASK = lo_mask(WORD)

def hi_mask(l):
    return MASK ^ lo_mask(WORD - l)

def u4(n):
    l = 18
    start = hi_mask(l) & n
    end = (start >> l) ^ (n & lo_mask(WORD - l))
    return (start + end) & MASK

#could be generalized into the right shift recovery function
def u1(n):
    l = 11
    pieces = [(hi_mask(l) & n) >> WORD - l]
    pieces.append(((n & lo_mask(WORD - l)) >> WORD - 2 * l) ^ pieces[-1])
    pieces.append((n & lo_mask(WORD - 2 * l)) ^ (pieces[-1] >> 3 * l - WORD))
    return ((pieces[0] << WORD - l) + (pieces[1] << WORD - 2 * l) + pieces[2]) & MASK

def u3(n):
    return u2(n, 0xefc60000, 15)

#y = y ^ ((y << l) & m) recovery func
def u2(n, m=0x9d2c5680, l=7):
    count = WORD // l
    pieces = [(lo_mask(l * (i + 1)) & n) >> l * i for i in xrange(count)] + [(hi_mask(count * l) & n) >> count * l]
    for i in xrange(count):
        pieces[i + 1] ^= pieces[i] & (m >> (i + 1) * l)

    return reduce(lambda x, y: (x << l) + y, pieces[::-1], 0) & MASK


def untemper(n):
    return u1(u2(u3(u4(n))))

if __name__ == "__main__":
    G = MT()
    G.seed(1)
    SMT = [untemper(G.next()) for i in xrange(CYCLE)]

    G2 = MT()
    G2.seed(1, SMT)

    print "Cloned generator prediction: ", G2.next()
    print "Actual generator next value: ", G.next()
