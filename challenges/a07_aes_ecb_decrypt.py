from Crypto.Cipher import AES
KEY = 'YELLOW SUBMARINE'

def decrypt(ct, key):
    aes = AES.new(key, AES.MODE_ECB)
    return aes.decrypt(ct)

if __name__ == "__main__":
    with open('./7.txt', 'r') as f:
        data = f.read().strip().decode('base64')
        print decrypt(data, KEY)
