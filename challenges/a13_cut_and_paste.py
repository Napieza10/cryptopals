from random import randint
from  Crypto.Cipher import AES
from a09_pkcs_padding import pad, unpad
from a11_detection_oracle import generate_key
from utils import chunk

KEY = generate_key()
aes = AES.new(KEY, AES.MODE_ECB)

def encrypt(s):
    return aes.encrypt(pad(s))

def decrypt(s):
    return unpad(aes.decrypt(s))

def parse(s):
    d = {}
    pairs = s.split('&')
    for pair in pairs:
        d.update([tuple(pair.split('='))])
    return d

def clean(s):
    s = s.replace('&', '')
    s = s.replace('=', '')
    return s

def profile_for(email):
    return 'email=' + clean(email) + '&uid=' + str(randint(1, 9999)) + '&role=user'

def forge():
    profile = profile_for('yulia@crypto.com')
    suffix = AES.block_size - (len(profile) % AES.block_size - len('user'))
    profile = profile[:len('email=')] + 'y' * suffix + profile[len('email='):]   
    print "Plaintext is: ", parse(profile)

    ct = encrypt(profile)[:-AES.block_size] + encrypt('admin')
    pt = parse(decrypt(ct))
    print "Decrypted plaintext is: ", pt

if __name__ == "__main__":
    forge()
