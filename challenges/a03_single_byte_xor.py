MOST_FREQUENT_CHAR = ' '

def get_unique_values(l):
    return list(set(l))

def score(c, l):
    return l.count(c)

def sort_by_score(l):
    unique = get_unique_values(l)
    return sorted(unique, key = lambda c: score(c, l), reverse=True)

def get_most_frequent(ct):
    return sort_by_score(ct)[0]

def get_key(ct):
    return ord(MOST_FREQUENT_CHAR) ^ get_most_frequent(ct)

def decrypt_with_key(s, key):
    return ''.join(map(chr, [i ^ key for i in s]))

def decrypt(ciphertext):
    ct = [ord(c) for c in ciphertext.decode('hex')]
    key = get_key(ct)
    pt = decrypt_with_key(ct, key)
    return pt

if __name__ == "__main__":
    print decrypt('1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736')
