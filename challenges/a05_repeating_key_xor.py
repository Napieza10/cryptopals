def encrypt_byte((b, k)):
    return hex(ord(b) ^ ord(k))[2:].zfill(2)

def keystream(key, l):
    key_len = len(key)
    times = l // key_len
    tail_size = l % key_len
    return key * times + key[:tail_size]

def encrypt(pt, key):
    ks = keystream(key, len(pt))
    return ''.join(map(encrypt_byte, zip(pt, ks)))

if __name__ == '__main__':
    plaintext = '''Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal'''
    key = 'ICE'
    print encrypt(plaintext, key)
