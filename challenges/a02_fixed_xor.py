def fixed_xor(s1, s2):
    l1 = [ord(c) for c in s1.decode('hex')]
    l2 = [ord(c) for c in s2.decode('hex')]
    return ''.join([chr(i ^ j) for i, j in zip(l1, l2)]).encode('hex')

if __name__ == "__main__":
    print fixed_xor('1c0111001f010100061a024b53535009181c', '686974207468652062756c6c277320657965').decode('hex')
