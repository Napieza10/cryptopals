from a11_detection_oracle import generate_key
from utils import chunk
from a09_pkcs_padding import pad
from Crypto.Cipher import AES
from utils import generate_prefix

UNKNOWN_STRING = '''Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg
                    aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq
                    dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg
                    YnkK'''

KEY = generate_key()

PREFIX = generate_prefix() #is solved for the length of the prefix up to 15 bytes

def encrypt(s):
    aes = AES.new(KEY, AES.MODE_ECB)
    return aes.encrypt(pad(PREFIX + s + UNKNOWN_STRING.decode('base64')))

def get_block(l, b, size=AES.block_size):
    return l[size * b: size * (b + 1)]

def get_second_identical_block(ct):
    for i in xrange(1, len(ct)):
        if ct[i - 1] == ct[i]:
            return i
    return -1

def discover_length():
    '''
    We send to the encrypter as much letters we need to have two identical encrypted blocks of A's.
    We start to make the sequence 1 byte shorter each step until the second of the blocks is changed.
    Now we have encrypted blocks such that the last letter of the corrupted block is the target letter.
    We can know the length of the prefix by 16 - (|sequence| - 31) = 47 - |sequence| for the block length = 16
    '''

    s = 'A' * 2 * AES.block_size
    ct = chunk(encrypt(s), AES.block_size)

    while (get_second_identical_block(ct) == -1):
        s += 'A'
        ct = chunk(encrypt(s), AES.block_size)

    idx = get_second_identical_block(ct)
    while(ct[idx - 1] == ct[idx]):
        s = s[:-1]
        ct = chunk(encrypt(s), AES.block_size)

    return 3 * AES.block_size - len(s) - 1


def recover():
    pt = ''
    prefix_length = discover_length()
    sup = AES.block_size - prefix_length
    actual = [encrypt('A' * (sup + i))[AES.block_size :] for i in xrange(AES.block_size)]

    for b in xrange(len(encrypt('')) // AES.block_size):
        for i in reversed(xrange(AES.block_size)):
            ct = get_block(actual[i], b)
            guess = [get_block( encrypt('A' * (sup + i) + pt + chr(c) )[AES.block_size:], b) for c in xrange(256)]
            if ct in guess:
                pt += chr(guess.index(ct))
    return pt[:-1]

    ''' after we have recovered the last byte of the unknown string,
    we try to recover the next byte cause we don't know it was the last byte.
    The last block of the sequence we form ('A'-sequence one byte shorter than before plus plaintext)
    is 1 byte shorter than the block must be so we always have \x01 padding recovered. '''

if __name__ == '__main__':
    print recover()
