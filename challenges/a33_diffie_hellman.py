from utils import modexp

class DiffieHellman(object):
    def __init__(self, prime, generator, order):
        self.prime = prime
        self.generator = generator
        self.order = order
        self.private_key = None
        self.public_key = None
        self.session_key = None

    def set_private_key(self, a):
        self.private_key = a % self.order

    def generate_public_key(self):
        self.public_key = modexp(self.generator, self.private_key % self.order, self.prime)

    def generate_session_key(self, b):
        # if h ** j == 0 return None
        # what if j % q == 0?
        self.session_key = modexp(b, self.private_key, self.prime)
        return self.session_key

