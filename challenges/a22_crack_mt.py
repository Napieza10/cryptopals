from time import sleep, time
from random import randint
from a21_rng import MT

MIN = 40
MAX = 1000

def wait():
    t = randint(MIN, MAX)
    sleep(t)

def random():
    wait()
    t = int(time())
    m = MT()
    m.seed(t)
    r = m.next()
    wait()
    return r

def crack(r):
    t = int(time())
    for i in xrange(MAX * 3):
        m = MT()
        m.seed(t - i)
        if m.next() == r:
            return t - i

if __name__ == "__main__":
    r = random()
    print crack(r)
