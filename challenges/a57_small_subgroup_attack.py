from a33_diffie_hellman import DiffieHellman
from random import randint
from utils import modexp

def product(l):
    return reduce(lambda x, y: x * y, l, 1)

def prime(n):
    for i in xrange(2, n / 2):
        if n % i == 0:
            return False
    return True

def find_factors(j, q, limit = 2 ** 16):
    factors = []
    last = 2
    while product(factors) <= q:
        last += 1
        if j % last == 0 and prime(last):
            factors.append(last)
    return factors

def find_element_by_order(p, r, q):
    h = 1
    while h == 1 or h % q == 0:
        h = modexp(randint(1, p), (p - 1) / r, p)
    return h

####################################

def CRT(n, a):
    res = 0
    prod = reduce(lambda a, b: a*b, n, 1)

    for n_i, a_i in zip(n, a):
        p = prod / n_i
        res += a_i * mul_inv(p, n_i) * p
    return res % prod


def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1:
        return 1
    while a > 1:
        q = a / b
        a, b = b, a%b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0:
        x1 += b0
    return x1

####################################

def recover_private_key(p, g, q, DH):
    j = (p - 1) / q
    factors = find_factors(j, q)
    secrets = []
    for r in factors:
        h = find_element_by_order(p, r, q)
        K = DH.generate_session_key(h)
        for i in xrange(r):
            if modexp(h, i, p) == K:
                secrets.append(i)
                break
    return CRT(factors, secrets)


if __name__ == "__main__":
    p = 7199773997391911030609999317773941274322764333428698921736339643928346453700085358802973900485592910475480089726140708102474957429903531369589969318716771
    g = 4565356397095740655436854503483826832136106141639563487732438195343690437606117828318042418238184896212352329118608100083187535033402010599512641674644143
    q = 236234353446506858198510045061214171961

    DH = DiffieHellman(p, g, q)
    DH.set_private_key(2300760005671)
    DH.generate_public_key()

    print recover_private_key(p, g, q, DH)

