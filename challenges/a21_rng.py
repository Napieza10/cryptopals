w = 32
n = 624
m = 397
r = 31

MASK = (1 << w) - 1


class MT(object):
    def __init__(self):
        self.idx = n + 1
        self.MT = [0] * n

    @staticmethod
    def get_xA(x):
        xA = x >> 1
        if x % 2 != 0:
            xA ^= 0x9908b0df
        return xA

    def twist(self):
        lo_MASK = (1 << r) - 1
        hi_MASK = 1 << r

        for i in xrange(n):
            x = (self.MT[i] & hi_MASK) + (self.MT[(i + 1) % n] & lo_MASK)
            self.MT[i] = self.MT[(i + m) % n] ^ self.get_xA(x)

    #SMT states for spliced MT
    def seed(self, s=5489, SMT = None):
        self.idx = n
    
        if SMT:
            self.MT = SMT
            return

        self.MT = [s & MASK]
        for i in xrange(1, n):
            self.MT.append(MASK & ((((self.MT[-1] >> (w - 2)) ^ self.MT[-1]) * 1812433253 + i)))


    def next(self):
        if self.idx > n:
            print "Generator was never seeded"
            print "Seeding with a predefined value..."
            self.seed()

        if self.idx == n:
            self.twist()
            self.idx = 0
    
        y = self.MT[self.idx]
        y = y ^ (y >> 11)
        y = y ^ ((y << 7) & 0x9D2C5680)
        y = y ^ ((y << 15) & 0xEFC60000)
        y = y ^ (y >> 18)
    
        self.idx += 1
        return y & MASK

if __name__ == "__main__":
    G = MT()
    print G.next()

