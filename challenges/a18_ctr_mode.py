from Crypto.Cipher import AES
from utils import sxor, chunk
import struct

NONCE = 0

KEY = "YELLOW SUBMARINE"
aes = AES.new(KEY, AES.MODE_ECB)

def little_endian(n):
    return struct.pack("<Q", n)

def generate_keystream(size):
    global NONCE
    keystream = ''.join([aes.encrypt(little_endian(NONCE) + little_endian(i)) for i in xrange(size)])
    NONCE += 1
    return keystream

def encrypt(pt):
    keystream = generate_keystream(len(pt) // AES.block_size + 1)
    return sxor(keystream, pt)

def decrypt(ct):
    return encrypt(ct)

if __name__ == "__main__":
    ct = "L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==".decode("base64")
    print decrypt(ct)

