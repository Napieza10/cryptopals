import pytest
from a33_diffie_hellman import DiffieHellman
from utils import modexp

def test_session_key_equality():
    p = '''ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff'''
    g = 2
    G = 0
    A = DiffieHellman(int(p, 16), g)
    B = DiffieHellman(int(p, 16), g)
    A.set_private_key(7)
    B.set_private_key(9)
    A.generate_public_key()
    B.generate_public_key()
    A.generate_session_key(B.private_key)
    B.generate_session_key(A.private_key)
    assert A.session_key == B.session_key

