from utils import s2b, chunk, write, sort_by_deviations
from single_byte_xor import decrypt as single_byte_xor_decrypt


MIN_KEYSIZE = 2
MAX_KEYSIZE = 40

NUMBER_OF_BEING_TESTED_KS = (MAX_KEYSIZE - MIN_KEYSIZE) / 4

def hamming(s1, s2):
    return sum([1 if c1 != c2 else 0 for c1, c2 in zip(s2b(s1), s2b(s2))])

def normalize(metric, val):
    return metric / float(val)

def metric(ct, size):
    NUMBER_OF_CHUNKS = 4

    chunks = chunk(ct, size, NUMBER_OF_CHUNKS)
    metric = (hamming(chunks[0], chunks[1]) + hamming(chunks[2], chunks[3])) / float(2)
    return normalize(metric, size)

def guess_keysize(ct):
    metrics = { ks : metric(ct, ks) for ks in xrange(MIN_KEYSIZE, MAX_KEYSIZE) }
    return sorted(metrics, key=metrics.get)[:NUMBER_OF_BEING_TESTED_KS]

def transpose(chunks, size):
    return [''.join( [c[i] if i < len(c) else '' for c in chunks]) for i in xrange(size)]

def compose(pts):
    size = len(pts[0])
    return ''.join(transpose(pts, size))

def known_keysize_decrypt(ct, keysize):
    chunks = chunk(ct, keysize)
    one_byte_xor_cts = transpose(chunks, keysize)
    pts = map(lambda c: single_byte_xor_decrypt(c.encode('hex')), one_byte_xor_cts)
    return compose(pts)
    
def decrypt(ct):
    keysizes = guess_keysize(ct)
    return sort_by_deviations(map(lambda ks: known_keysize_decrypt(ct, ks), keysizes))[0] #sorting by deviations could be removed; 
    #removing idx, you get plaintexts for all probable values of the keysize

if __name__ == '__main__':
    with open('./6.txt', 'r') as f:
        ct = f.read().strip().decode('base64')
        pt = sort_by_deviations(decrypt(ct))

        map(lambda (idx, p) : write('./results/res' + str(idx), p), enumerate(pt))
