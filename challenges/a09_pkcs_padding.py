AES_BLOCK_SIZE = 16

def pad(pt, size=AES_BLOCK_SIZE):
    length = - len(pt) % size
    return pt + length * chr(length)

def has_valid_padding(pt, size=AES_BLOCK_SIZE):
    last_block = pt[-AES_BLOCK_SIZE:]
    last_byte = ord(last_block[-1])
    if last_byte >= 0x01 and last_byte <= 0x10:        
        return all([ ord(b) == last_byte for b in last_block[-last_byte:] ])

    return False #unpadded pt are not allowed for now

def unpad(pt, size = AES_BLOCK_SIZE):
    if not has_valid_padding(pt, size):
        raise "Invalid padding"
    
    last_block = pt[-AES_BLOCK_SIZE:]
    last_byte = ord(last_block[-1])

    if last_byte == 0 or last_byte >= size:
        return pt
    return pt[:-AES_BLOCK_SIZE] + last_block[:-last_byte]

if __name__ == "__main__":
    print has_valid_padding(pad("YELLOW SUBMARINE", 16))
    print has_valid_padding(pad("YELLOW SUBMARIN", 16), 16)
    print has_valid_padding(pad("YELLOW SUBMARI", 16), 16)
    print has_valid_padding(pad("YELLOW SUBMA", 16), 16)
