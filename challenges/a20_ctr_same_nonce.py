from a06_vigenere import decrypt

def trunkate(l):
    m = min(map(len, l))
    return [e[:m] for e in l]

def recover(l):
    ct = ''.join(trunkate(l))
    return decrypt(ct)

if __name__ == "__main__":
    with open("20.txt", "r") as f:
        ct = f.read().strip().split('\n')
        pt = recover(map(lambda x: x.decode('base64'), ct))
        print pt
