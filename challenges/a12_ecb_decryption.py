from a11_detection_oracle import generate_key
from utils import chunk
from a09_pkcs_padding import pad
from Crypto.Cipher import AES

UNKNOWN_STRING = '''Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg
                    aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq
                    dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg
                    YnkK'''

KEY = generate_key()
def encrypt(s):
    aes = AES.new(KEY, AES.MODE_ECB)
    return aes.encrypt(pad(s + UNKNOWN_STRING.decode('base64')))

def get_block(l, b, size=AES.block_size):
    return l[size * b: size * (b + 1)]

def recover():
    pt = ''
    actual = [encrypt('A' * i) for i in xrange(AES.block_size)]
    for b in xrange(len(encrypt('')) // AES.block_size):
        for i in reversed(xrange(AES.block_size)):
            ct = get_block(actual[i], b)
            guess = [get_block( encrypt('A' * i + pt + chr(c) ), b) for c in xrange(256)]
            if ct in guess:
                pt += chr(guess.index(ct))
    return pt[:-1]

    ''' after we have recovered the last byte of the unknown string,
    we try to recover the next byte cause we don't know it was the last byte.
    The last block of the sequence we form ('A'-sequence one byte shorter than before plus plaintext)
    is 1 byte shorter than the block must be so we always have \x01 padding recovered. '''

if __name__ == '__main__':
    print recover()
