from random import randint
from Crypto.Cipher import AES
from a09_pkcs_padding import pad, has_valid_padding as check_padding
from a11_detection_oracle import generate_key
from utils import chunk, sxor, bxor


KEY = generate_key()
IV = generate_key()
aes = AES.new(KEY, AES.MODE_CBC, IV)

STRINGS = ["MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=",
"MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=",
"MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==",
"MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==",
"MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl",
"MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==",
"MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==",
"MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=",
"MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=",
"MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93"]

def encrypt(s):
    return aes.encrypt(pad(s))

def decrypt(s):
    return aes.decrypt(s)

def generate_ct():
    idx = randint(0, len(STRINGS) - 1)
    s = STRINGS[idx].decode('base64')
    return encrypt(s)

def has_valid_padding(ct):
    pt = decrypt(ct)
    return check_padding(pt)

def fill(s, length, val='\x00'):
    return val * length + s

def absorb_pt(b, pt, pad):
    ms = fill(bxor(pt, pad), AES.block_size - pad + 1, '\xff')
    return sxor(b, ms)

def absorb_byte(b, val, idx):
    ms = fill(chr(val ^ idx) + '\x00' * (idx - 1), AES.block_size - idx)
    return sxor(b, ms)

def guess_byte(cur_block, pad, prev_block):
    for g in xrange(256):
        ct = absorb_byte(prev_block, g, pad) + cur_block
        if has_valid_padding(ct):
            return chr(g ^ 0xff)

    raise Exception("Couldn't find correct value")

def crack_block((prev_block, cur_block)):
    pt = ''
    for pad in xrange(1, AES.block_size + 1):
        modified = absorb_pt(prev_block, pt, pad)
        pt = guess_byte(cur_block, pad, modified) + pt
    return pt

def crack(ct):
    blocks = [aes.IV] + chunk(ct, AES.block_size)
    pt = [crack_block(blocks[i - 1:i + 1]) for i in reversed(xrange(1, len(blocks)))][::-1]
    return ''.join(pt)

if __name__ == "__main__":
    ct = generate_ct()
    print [crack(ct)]
