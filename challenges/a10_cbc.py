from Crypto.Cipher import AES
from a09_pkcs_padding import pad
from a02_fixed_xor import fixed_xor as xor
from utils import chunk

#[in] strings
#[out] strings
def sxor(s1, s2):
    return xor(s1.encode('hex'), s2.encode('hex')).decode('hex')

#[in] string
#[out] hex encoded string
def cbc_encrypt(pt, key, iv):
    pt = chunk(pad(pt), AES.block_size)
    aes = AES.new(key, AES.MODE_ECB)
    ct = [iv]

    for t in pt:
        xored = sxor(t, ct[-1])
        block = aes.encrypt(xored)
        ct.append(block)
    return ''.join(map(lambda x: x.encode('hex'), ct))


#[in] hex encoded string
#[out] string
def cbc_decrypt(ct, key):
    ct = chunk(ct.decode('hex'), AES.block_size)
    aes = AES.new(key, AES.MODE_ECB)
    pt = []
    for i in xrange(len(ct) - 1, 0, -1):
        xored = aes.decrypt(ct[i])
        pt.append(sxor(xored, ct[i - 1]))
    return ''.join(pt[::-1])

if __name__ == "__main__":
    pt = "hi there i'm going to test if you can encrypt and decrypt long enough messages as well, sir"
    ct = cbc_encrypt(pt, "YELLOW SUBMARINE", "test iv val, sir")
    new_pt = cbc_decrypt(ct, "YELLOW SUBMARINE")
    print "befor encryption: ", pt
    print "after decryption: ", new_pt
