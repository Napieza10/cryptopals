from Crypto.Cipher import AES
from os import urandom
from random import randint, choice
from utils import chunk
from a09_pkcs_padding import pad

def generate(size):
    return urandom(size)

def generate_key():
    KEYSIZE = 16
    return generate(KEYSIZE)

def get_length():
    MIN_SIZE = 5
    MAX_SIZE = 10
    return randint(MIN_SIZE, MAX_SIZE)

def cover(data):
    prefix = generate(get_length())
    suffix = generate(get_length())
    return prefix + data + suffix

def get_mode():
    return choice([AES.MODE_ECB, AES.MODE_CBC])


def encrypt_under_unknown_key(pt):
    key = generate_key()
    iv = generate(AES.block_size)
    mode = get_mode()

    if mode == AES.MODE_CBC:
        print("Encrypted using CBC mode")
        aes = AES.new(key, mode, iv)
    else:
        print("Encrypted using ECB mode")
        aes = AES.new(key, mode)
    
    return aes.encrypt(pad(cover(pt)))

def oracle():
    HEX_BLOCK_SIZE = AES.block_size * 2
    TEST_PLAINTEXT = "A" * AES.block_size * 4

    ct = encrypt_under_unknown_key(TEST_PLAINTEXT).encode('hex')
    blocks = chunk(ct, HEX_BLOCK_SIZE)
    
    if (blocks[1] == blocks[2]) and (blocks[2] == blocks[3]):
        print("Detected mode:  ECB\n")
        return

    print("Detected mode:  CBC\n")


if __name__ == "__main__":
    for i in range(10):
        oracle()
